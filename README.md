# Aplikace neuronových sítí

## Úlohy: deadline na uzavření předmětu je **29.6.2018**!

### 01 Lineární klasifikace
- Notebook: [linear-classify-lab.ipynb](linear-classify-lab.ipynb)
- Softmax trénování + validace: 7 bodů
- Bonus SVM: 3 body
- **deadline: 3.4.2018 8:00**

### 02 Vícevrstvé sítě
- Notebook: [multilayer-classes-lab.ipynb](multilayer-classes-lab.ipynb)
- Softmax cross entropy, dvouvrstvá a vícevrstvá síť: 8 bodů
- Bonus ladění: 2 body
- **deadline: 11.4.2018 8:00**

### 03 Konvoluce
- Notebook: [conv-intro-lab.ipynb](conv-intro-lab.ipynb)
- Konvoluce po kanálech, s více filtry, PyTorch: 5 bodů
- Dosažené validační skóre *vlastní* síťí (nejsou povolené předtrénované sítě!):
    - < 70 %: 0 bodů
    - 70 - 74.99 %: 2 body
    - 75 - 79.99 %: 4 body
    -  \> 80 %: 5 bodů
- **deadline: 25.4.2018 8:00**

### 04 Generování textu znakovou RNN
- Notebook: [char-rnn-lab.ipynb](char-rnn-lab.ipynb)
- Úspěšné vygenerování textu: 10 bodů
- Pozn: nemusí fungovat hezky
    - nebude uznáno : `"prezident5tz5h2a96gasj zqqpunb8"`
    - bude uznáno: `"kausta uzavira slovensky vyhnale italska"`
- **deadline: 2.5.2018 8:00**

### 05 Generování textu slovní RNN (bonusová nepovinná úloha)
- Použijte předtrénované slovní vektory a natrénujte síť pro generování textu po slovech
- Word-vektory pro Češtinu lze [získat např. zde](https://fasttext.cc/docs/en/pretrained-vectors.html)
- Úspěšné vygenerování textu: 10 bodů

### 06 Adversarial examples
- Notebook: [adversarial-examples-lab.ipynb](adversarial-examples-lab.ipynb)
- Vytvoření adversarialu 32x32: 6 bodů
- Použití vlastního modelu namísto předtrénovaného: 2 body
- Vytvoření adversarialu v původním rozlišení: 2 body
- Pokud bude adversarial obrázek příliš odlišný od původního (vysoká MSE), mohou být odečteny body
- **deadline: 16.5.2018 8:00**

### 07 Neural style
- Notebook: [neural-style-lab.ipynb](neural-style-lab.ipynb)
- Výměna max-poolingů za avg-pooling: 2 body
- Správné nastavení vah: 3 body
- Funkční transfer: 5 bodů
- Dohromady: 10 bodů
